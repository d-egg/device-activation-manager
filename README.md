Device Activation Manager
=========================

Pass a token around your device fleet to ensure that only one device is
considered active. Use this to lock Git directories and reduce the potential for
synchronization conflicts.

### Example Use Case

You synchronize a desktop and a laptop but want to have a little automated
assistance to prevent sync conflicts. You are productive on only one device at
a time and have specific moments where you switch the device.

### Dedicated exchange versus network partitions

There are two parts to this.

On the one hand, there is a dedicated exchange between two devices. This is
where you might want to have additional things happen like storing editor
sessions and verifying the synchronization state. To facilitate developing
a routine, you can configure an inert host, for example, your desktop, and then
get/put the token from the laptop when you leave/come.  This part is called the
activation state (AS) and is machine-specific. It relies on SSH with public key
authentication.

On the other hand, there is a synchronized record, called event tokens (ET),
where devices can react to changes and verify that they are in the right state.
When you forget to have an explicit switch, you can synchronize with your
always-on smartphone and force the laptop to be active. As soon as the desktop
sees the forced token it will turn itself off.  You can use a `systemd.path`
handler to call into this program and establish that.

### Git external directories

Initially, I used a symlink in several Git external directory specifications to
turn Git off by breaking that symlink. That is still the mechanism by which the
active state is determined. If you don't want to use that, it is just a file in
an otherwise empty directory that gets renamed from `syncer.on` to `syncer.off`
and back.

Put a git-wrapper early in your `$PATH` to make it behave as if there is no
`.git` directory anywhere.

### Source code

My plan was to try some newer bash features for this and see how far I can get
to something useful. For the most part, I'm using pass-by-reference
(`declare/local -n`) and associative arrays. It's still a bag of tricks and you
probably want to read it all before using it. I thought it could be done in 10
SLOC, too.

There is a configuration section at the top of the file and you can override
that with a local file.

**Requirements**

- SSH
- jq
- Bash-4.3
- Syncthing or other
- systemd or other
- Kdialog or other

### Usage

Copy the scripts `device-activation.sh` and `dam-bash-tools.sh` somewhere into
your `$PATH` and create a configuration `device-activation.rc` next to it.
Let's assume the main script is available under the name `dam` and there is
a configuration file with this content:

    AS_folder="/home/you/var/act-state/"
    AS_link_target="/home/you/src/"
    ET_folder="/home/you/src/Syncthing/work/events/"
    ET_filename="tokens"
    inert_host_cfg="desk"

If you now call `init-device` on your desktop `desk` it will create these two
folders and files:

    /home/you/var/act-state/syncer.on -> /home/you/src/
    /home/you/src/Syncthing/work/events/tokens

The first file is the symbolic link used to determine the activation state of
a particular device.  The `tokens` file will look like this:

    {
        "active": "desk",
        "forced": ""
    }

The `active` entry determines the globally active host and the `forced` record
will contain the name of the host that overrides that active state in case the
active host isn't available for a dedicated exchange. More on that later.

The `tokens` record is supposed to be synchronized to all partaking devices.
When you call `init-device` on your laptop, only the activation state will be
created and it will start in a `syncer.off` state.

When you want to leave with your laptop, you can bracket your own sync tasks in
in calls to `check-get/get` and do the same for coming back with
`check-put/put`.

    leave() { \
        dam check-get && \
        my-sync-tasks frominerthost && \
        dam get ; }

    come() { \
        dam check-put && \
        my-sync-tasks toinerthost && \
        dam put ; }

As insinuated above, you can use your smartphone as some sort of NAS.  When you
happen to forget to switch over explicitly you might still know if you are in
a good state after syncing with the phone and can force the laptop active by
calling

    dam force-active

To resolve this conflict recorded in the tokens record, I recommend calling the
`resolve` sub-command whenever there are changes to that record. The previously
active host will turn itself off and create a normal record for the now active
host.

    dam resolve

This call also checks whether the local host corresponds to the announced global
state when there is no conflict recorded.  There are `systemd.path` templates in
the `util` folder that you can use to install as a user service.

You might want to get informed about resolution or errors from the handler.
Here is an example configuration that uses `notify-send`.

    call_no_se() { notify-send --expire-time=0 --app-name="$1" " " "$2" ; }
    errmsg_generator="call_no_se"

Now you can lock selected features of your device based on the symlink in the
activation state.  There is a template script for a `git` executable in `util`
that you can use to disable Git when it is on an inactive device.

Alternatively, you can move the `.git` directory of any repository to, for
example

    /home/you/src/Syncthing/work/my-repo-gitdir

and then link back to it by creating a `.git` file that contains

    gitdir: /home/you/var/act-state/syncer.on/Syncthing/work/my-repo-gitdir

When the symlink gets renamed to `syncer.off` the external git directory will no
longer be available.
