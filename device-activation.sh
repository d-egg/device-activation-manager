#!/bin/bash

# this program and its location
declare -r script_full_path="$(readlink -f "$0")"
declare -r programname="$(basename "${script_full_path}")"
declare -r source_directory="$(dirname "${script_full_path}")"

# This can be a tool to create GUI error messages. Required to see whether the
# resolution handler works as expected.  You can again use a synchronized
# location to write a file and let, eg, systemd.path pick it up to also get
# notified about events from other devices.
#
# Arguments to it are: $programname <gui_message>
errmsg_generator="call_kdialog"

# The configuration for the device-specific record of whether the particular
# device is in Active-State or not. It may not be synchronized obviously. It
# started out as being a symlink that can be broken when inserted into Git
# external directory specifications. Nobody can find it. If anything actually
# works with a git directory directly, this might be a better but more arduous
# lock. The folder needs to be empty otherwise.
AS_folder="${HOME}/device-activation-manager/"
AS_link_target="../src/"

# The folder and filename for the events token record. It should be synchronized
# and thus shared by all devices. It is used to inform all devices about the
# current state so that they can adjust if necessary.
ET_folder="${HOME}/Syncthing/"
ET_filename="activation-token"

# The name of a host that you consider your central hub. This could be your
# desktop and you get/put the active setting from/to it.
inert_host_cfg=""

# This is localhost if left empty (recommended). The idea is to get a user's
# routine where you leave/come relative to the inert host. But this can be used
# for testing or to get any other policy implemented. Note that you can use
# environment variables in the config.
operative_host_cfg=""

# Hostnames read/written from/to the token record are checked for ASCII
# alpha-numeric plus hyphen.  Turn that check off by setting this to 0.
hostname_sanity_check=1

# This is where you can put your custom configuration for the variables above
dam_configuration_file="${source_directory}/device-activation.rc"

#-----------------
#--- Custom config
#-----------------

# Source the local configuration
if [ ! -r "$dam_configuration_file" ] ; then
    echo "Missing a custom configuration at $dam_configuration_file"
    exit 1
fi
source "$dam_configuration_file"

# Expand tilde in the configuration
AS_folder="${AS_folder/#\~/$HOME}"
AS_link_target="${AS_link_target/#\~/$HOME}"
ET_folder="${ET_folder/#\~/$HOME}"

#------------
#--- Internal
#------------

# Source some color output tools
source "${source_directory%/}/dam-bash-tools.sh"

# activation states. Ensure they are bit flags to make state computations
declare -ir act_state_on=$(( 1<<0))
declare -ir act_state_off=$((1<<1))
declare -ir act_state_combo_valid=$(( $act_state_on + $act_state_off ))
declare -ir act_state_combo_missing=$(( $act_state_off + $act_state_off ))
declare -ir act_state_combo_conflict=$(( $act_state_on + $act_state_on ))

declare -rA AS_link_name=(
    [$act_state_on]="syncer.on"
    [$act_state_off]="syncer.off"
)

declare -rA AS_display_name=(
    [$act_state_on]="active"
    [$act_state_off]="inactive"
)

# full paths
AS_full_path_on="${AS_folder%/}/${AS_link_name[$act_state_on]}"
AS_full_path_off="${AS_folder%/}/${AS_link_name[$act_state_off]}"
ET_full_path="${ET_folder%/}/${ET_filename}"

# lower case aliases
success=$EXIT_SUCCESS
failure=$EXIT_FAILURE

thishost=`hostname`

# (also) for debug_out()
DEBUG=${DEBUG:-0}

#---
#--- Tools
#=========
#---

# An example for the errmsg_generator
call_kdialog() { kdialog --title "$1" --msgbox "$2" ; }

usage() {
        cecho white blue "USAGE: $(basename $0) [-h|--help] <cmd>"
        echo "Switch user interaction to/from inert host. Mainly, to"\
            "ensure Git interaction happens only on a specific device."
        [[ $# -gt 0 ]] && exit $1
}

# The key is to have lines starting with '# -' or '# ='. The comments after the
# case parentheses will be the commands and help output. Empty lines have just
# a hash.
help_output() {
    usage
    echo
    grep -e '^\s*# [=-]' -e ')\ # ' -e '#$' "`readlink -f "$0"`" \
        | sed 's/^# \?//g;s/) #/                                    /g' \
        | sed 's/^\(.\{34\}\)\s\{2,\}/\1/g'
    echo
}

is_interactive_shell() {
    local stdin_fd=0
    [[ -t $stdin_fd || -p /dev/stdin ]]
}

is_dumb_terminal() {
    [[ $TERM =~ unknown|dumb || $TERM == "" ]]
}

exitc() {
    # Abort with a colorful message
    local exitcode=$1
    shift
    local tui_msg="$@"
    local gui_message=`echo "<font style=\"color:red\"><b>"\
        "${programname}</b></font><br><br>$@<br>"`

    if is_interactive_shell ; then
        cerror white red "$tui_msg"
        # I don't trust it
        if is_dumb_terminal ; then
            disaster_msg="ERROR: Pretty sure I'm in a dam handler. Message: "
            $errmsg_generator "$programname" "${disaster_msg}${gui_message}"
        fi
    else
        echo "$tui_msg"
        $errmsg_generator "$programname" "$gui_message"
    fi
    exit "$exitcode"
}

notify() {
    # notify in color or with the errmsg_generator. Eg resolution.
    local tui_msg="$@"
    local gui_message=`echo "<font style=\"color:blue\"><b>"\
        "${programname}</b></font><br><br>$@<br>"`

    if is_interactive_shell ; then
        cecho blue white "$@"
    else
        echo "$tui_msg"
        $errmsg_generator "$programname" "$gui_message"
    fi
}

#---

enforce_number_of_arguments() {
    [ $# -eq 3 ]  || exitc 1 "$FUNCNAME not called with 3 arguments"
    [ $1 -eq $2 ] || exitc 1 "$3 not called with $1 argument(s) but $2"
}

# Meh. This conflict search is syncthing specific.
enforce_lack_of_sync_conflicts_in_events_folder() {
    if [ `find "$ET_folder" -iname "*sync-conflict*" | wc -l` -ne 0 ] ; then
        exitc 1 "There are conflicts in $ET_folder"
    fi
}

enforce_lack_of_forced_token() {
    # 1: the activation state check
    enforce_number_of_arguments 1 $# $FUNCNAME
    local -n et_ref_ref=$1

    if [ -n "${et_ref_ref[forced]}" ] ; then
        exitc 1 "Forced token exists. Refusing operation."
    fi
}

enforce_clean_state() {
    # 1: the activation state check
    enforce_number_of_arguments 1 $# $FUNCNAME
    local -n et_ref=$1

    enforce_lack_of_forced_token et_ref
    enforce_lack_of_sync_conflicts_in_events_folder
}

enforce_inert_host_is_specified() {
    if [ -z "$inert_host" ] ; then
        exitc 1 "Operation '$subcommand' does not make sense without an"\
            "interlocutor. Operative host is '$operative_host'."
    fi
}

enforce_act_const_is_filled() {
    # 1: the activation constellation to check
    enforce_number_of_arguments 1 $# $FUNCNAME
    local -n ac_rr=$1
    [ ${#ac_rr[@]} -eq 4 ] || exitc 1 "$FUNCNAME act constellation not filled"
}

enforce_hostname_chars() {
    # 1: the hostname to check
    local hostspec=$1
    if [ $hostname_sanity_check -eq 0 ] ; then
        debug_out "Not sanity checking the hostname."
        return
    fi
    if echo "$hostspec" | LC_ALL=C grep -qP "[^A-Za-z0-9-]" ; then
        exitc 1 "A hostname contains invalid characters. You can"\
            "turn this check off in the configuration."
    fi
}

#---

get_first_word() {
    # strip first word from string
    echo $1 | head -1 | awk '{print $1}'
}

is_localhost() {
    # 1: host to query
    enforce_number_of_arguments 1 $# $FUNCNAME
    local host_q=$1
    [[ "$host_q" == "`hostname`" ]] && \
        return $success || \
        return $failure
}

#---

#---
#--- Activation Constellation
#============================
#---

get_activation_state() {
    # Get the activation state constant for one host
    # 1: host to query
    enforce_number_of_arguments 1 $# $FUNCNAME
    local host_q=$1
    local ls_answer ls_items link_name state_i

    # query remote or local device
    if is_localhost "${host_q}" ; then
        ls_answer="`ls -1 ${AS_folder}`"
    else
        ls_answer="`ssh "$host_q" "ls -1 ${AS_folder}"`"
    fi
    if [ $? -ne 0 ] ; then
        exitc 1 "Error querying active-device directory on $host_q"
    fi

    # create an array from the command output
    mapfile -t ls_items <<< "${ls_answer}"

    # ensure there is just one directory entry
    if [ ${#ls_items[@]} -eq 1 ] ; then
        link_name="${ls_items[0]}"
    else
        exitc 1 "active-device directory \"${AS_folder}\" on"\
            "$host_q has an invalid state: ${#ls_items[@]}"\
            "entries instead of 1"
    fi

    # figure out the state constant and return it
    for state_i in "${!AS_link_name[@]}" ; do
        if [ "$link_name" == "${AS_link_name[$state_i]}" ] ; then
            return $state_i
        fi
    done

    exitc 1 "active-device directory \"${AS_folder}\" on"\
        "$host_q has an invalid state: ${link_name}"\
        "is not a recognized state"
}

fill_activation_constellation_structure() {
    # 1: dictionary to be filled with activation constellation
    enforce_number_of_arguments 1 $# $FUNCNAME
    local -n act_cons_ref=$1

    enforce_inert_host_is_specified

    act_cons_ref[operative]="${operative_host}"
    act_cons_ref[inert]="${inert_host}"

    get_activation_state "${act_cons_ref[operative]}"
    act_cons_ref[operstate]=$?

    get_activation_state "${act_cons_ref[inert]}"
    act_cons_ref[inertstate]=$?
}

display_activation_constellation() {
    # 1: activation constellation structure to be displayed
    enforce_number_of_arguments 1 $# $FUNCNAME
    local -n ac_ref=$1
    enforce_act_const_is_filled ac_ref

    echo "${ac_ref[operative]} : ${AS_display_name[${ac_ref[operstate]}]}"
    echo "${ac_ref[inert]} : ${AS_display_name[${ac_ref[inertstate]}]}"
}

check_activation_combo() {
    # Get and display the state of the interlocutors
    # 1: activation constellation to check
    enforce_number_of_arguments 1 $# $FUNCNAME
    local -n ac_r=$1

    enforce_act_const_is_filled ac_r

    combined_state=$(( ${ac_r[inertstate]} + ${ac_r[operstate]} ))
    if [ $combined_state -eq $act_state_combo_valid ] ; then
        debug_out "Valid state among ${ac_r[operative]} and ${ac_r[inert]}"
        return $success
    elif [ $combined_state -eq $act_state_combo_missing ] ; then
        notify "No active state found on either ${ac_r[operative]}"\
            "or ${ac_r[inert]}. Ask myself: Where is the token?"
        return $failure
    elif [ $combined_state -eq $act_state_combo_conflict ] ; then
        exitc 2 "Conflict: Both ${ac_r[operative]} "\
            "and ${ac_r[inert]} are in an active state."
    else
        exitc 2 "(internal) Unknown state combo $combined_state encountered."
    fi
}

switch_device_actstate() {
    # Turn the device-active link into on/off state by renaming it
    # 1: host
    # 2: activation state
    enforce_number_of_arguments 2 $# $FUNCNAME
    local host=$1
    local state_wanted=$2
    local new_link_name old_link_name new_link_path old_link_path

    # get new link name
    if [ $state_wanted -eq $act_state_on ] ; then
        new_link_name="${AS_link_name[$act_state_on]}"
        old_link_name="${AS_link_name[$act_state_off]}"
    elif [ $state_wanted -eq $act_state_off ] ; then
        new_link_name="${AS_link_name[$act_state_off]}"
        old_link_name="${AS_link_name[$act_state_on]}"
    else
        exitc 1 "$FUNCNAME invalid state requested"
    fi

    new_link_path="${AS_folder%/}/${new_link_name}"
    old_link_path="${AS_folder%/}/${old_link_name}"

    if is_localhost "${host}" ; then
        mv "$old_link_path" "$new_link_path"
    else
        ssh "$host" "mv '$old_link_path' '$new_link_path'"
    fi
    if [ $? -ne 0 ] ; then
        exitc 1 "Error creating new symlink on $host"
    fi
}

toggle_active_device() {
    # Switch the active device to/from inert host
    # 1: activation constellation structure
    # 2: direction specifier
    enforce_number_of_arguments 2 $# $FUNCNAME

    local -n ac=$1
    local direction=$2

    local combined_state
    combined_state=$(( ${ac[inertstate]} + ${ac[operstate]} ))
    if [ $combined_state -ne $act_state_combo_valid ] ; then
        exitc 2 "$FUNCNAME: should not happen. Not a valid combo"
    fi

    case "${direction}" in

        toinerthost)
            if [ ${ac[inertstate]} -eq $act_state_on ] ; then
                notify "Nothing to do. Host ${ac[inert]}"\
                        "is already active."
                return
            fi

            notify "Switching from ${ac[operative]} to ${ac[inert]}..."
            switch_device_actstate "${ac[operative]}" $act_state_off
            switch_device_actstate "${ac[inert]}" $act_state_on

            write_event_tokens "${ac[inert]}" ""

            ;;

        frominerthost)
            if [ ${ac[operstate]} -eq $act_state_on ] ; then
                notify "Nothing to do. Host ${ac[operative]}"\
                        "is already active."
                return
            fi

            notify "Switching from ${ac[inert]} to ${ac[operative]}..."
            switch_device_actstate "${ac[inert]}" $act_state_off
            switch_device_actstate "${ac[operative]}" $act_state_on

            write_event_tokens "${ac[operative]}" ""

            ;;

        *)
            exitc 1 "${FUNCNAME}: invalid sub-command (internal)"
            ;;
    esac
}

#---
#--- Events Token
#================
#---

write_event_tokens() {
    # 1: active device
    # 2: forced device or empty string
    enforce_number_of_arguments 2 $# $FUNCNAME

    enforce_hostname_chars "$1"
    enforce_hostname_chars "$2"

    local active_device=`get_first_word $1`
    local forced_device=`get_first_word $2`

    [ -n "$active_device" ] || \
        exitc 1 "$FUNCNAME: active_device may not be empty"

    jq -n \
        --arg active "$active_device" \
        --arg forced "$forced_device" \
        '{"active":$active, "forced":$forced}' > "$ET_full_path"
}

read_event_tokens() {
    # 1: dictionary to be filled with events data regarding the activity tokens
    enforce_number_of_arguments 1 $# $FUNCNAME
    local -n et_ref=$1

    [ -f "$ET_full_path" ] || exitc 1 "$FUNCNAME: activation state file"\
        "does not exist ($ET_full_path)"

    # The currently active host, Guaranteed not empty (ATM of writing this):
    et_ref[active]=`jq --exit-status --raw-output ".active" "$ET_full_path"`
    if [[ $? != 0 || "${et_ref[active]}" == "null" ]] ; then
        exitc 1 "Error reading 'active' from ($ET_full_path)"
    fi
    enforce_hostname_chars "${et_ref[active]}"

    # Empty or contains the name of the forced host:
    et_ref[forced]=`jq --exit-status --raw-output ".forced" "$ET_full_path"`
    if [[ $? != 0 || "${et_ref[forced]}" == "null" ]] ; then
        exitc 1 "Error reading 'forced' from ($ET_full_path)"
    fi
    enforce_hostname_chars "${et_ref[forced]}"

    if [ -z "${et_ref[active]}" ] ; then
        exitc 1 "active device is empty in ($ET_full_path)"
    fi
}

#---

display_event_tokens() {
    # 1: the activation state to display
    enforce_number_of_arguments 1 $# $FUNCNAME
    local -n et_ref=$1

    debug_out "Tokens:"
    echo "- active: ${et_ref[active]}"
    echo "- forced: ${et_ref[forced]}"
}

enforce_validity_of_localhost_state() {
    # Check the state is as expected for localhost and turn it off if necessary.
    # 1: actual state
    # 2: expected state
    enforce_number_of_arguments 2 $# $FUNCNAME
    local act_state=$1
    local expected_state=$2

    # Well, ...
    local msg_on=${AS_display_name[$act_state_on]}
    local msg_off=${AS_display_name[$act_state_off]}

    if [ $act_state -eq $expected_state ] ; then
        debug_out "Valid: $thishost is ${AS_display_name[$expected_state]}."
    else
        if [ $act_state -eq $act_state_on ] ; then
            switch_device_actstate $thishost $act_state_off
            exitc 1 "Invalid: $thishost was expected to be $msg_off"\
                "but wasn't. Turned it ${msg_off}."
        else
            exitc 1 "Invalid: $thishost should be ${msg_on} but isn't."
        fi
    fi
}

check_event_tokens_and_resolve_cond() {
    # Check event token contents validity regarding the state of localhost.
    # Resolve if appropriate and wanted. (Resolution is supposed to be done by
    # the handler)
    # 1: event tokens structure of the current state
    # 2: nonempty-yes if you want to resolve a forced state
    enforce_number_of_arguments 2 $# $FUNCNAME
    local -n et=$1
    local want_resolution=$2

    enforce_lack_of_sync_conflicts_in_events_folder

    get_activation_state "$thishost"
    act_state=$?

    # Fail if active/forced token are equal
    if [ "${et[forced]}" == "${et[active]}" ] ; then
        exitc 1 "Should not happen: active/forced tokens are both"\
            "'${et[active]}/${et[forced]}'. This host is '${thishost}'"
    fi

    if [ -n "${et[forced]}" ] ; then

        # Handle a forced state
        #----------------------

        echo "'${et[forced]}' is forcing the active state away "\
            "from '${et[active]}'. This host is '${thishost}'"

        # Check localhost's role in the forced constellation
        if [ "${et[forced]}" == "$thishost" ] ; then
            echo "This host ($thishost) is forcing."
            enforce_validity_of_localhost_state $act_state $act_state_on
        elif [ "${et[active]}" == "$thishost" ] ; then
            echo "This host ($thishost) is being forced."
            enforce_validity_of_localhost_state $act_state $act_state_on

            # Resolution
            #-----------
            if [ -n "$want_resolution" ] ; then
                echo "Resolving forced state away from '$thishost'"
                switch_device_actstate $thishost $act_state_off
                write_event_tokens "${et[forced]}" ""
                notify "Resolved forced state. Deactivated localhost"\
                    "'$thishost'"
            fi
        else
            echo "Two other hosts are in conflict."
            enforce_validity_of_localhost_state $act_state $act_state_off
        fi
    else

        # Handle a normal state
        #----------------------

        # Find inconsistency with the recorded state and this host
        if [ "${et[active]}" == "$thishost" ] ; then
            debug_out "This host ($thishost) is recorded as on."
            enforce_validity_of_localhost_state $act_state $act_state_on
        else
            debug_out "This host ($thishost) is recorded as off."
            enforce_validity_of_localhost_state $act_state $act_state_off
        fi
    fi

}

#---
#--- Sub-commands
#================
#---

to_from_inerthost() {
    # 1: direction specifier
    enforce_number_of_arguments 1 $# $FUNCNAME
    enforce_inert_host_is_specified
    local direction=$1

    local -A et_tofrom
    read_event_tokens et_tofrom
    local -r et_tofrom
    enforce_clean_state et_tofrom

    local -A ac_tofrom
    fill_activation_constellation_structure ac_tofrom
    local -r ac_tofrom

    if [ $DEBUG -eq 1 ] ; then
        echo "Before:"
        display_activation_constellation ac_tofrom
    fi
    check_activation_combo ac_tofrom
    if [ $? -ne $success ] ; then
        exitc 1 "Cannot switch. Not a valid combo."\
            "Token is at ${et_tofrom[active]}"
    fi

    # rename switch_device_actstate or this one
    # toggle-combo or so
    toggle_active_device ac_tofrom $direction

    local -A ac_tofrom_post
    fill_activation_constellation_structure ac_tofrom_post
    local -r ac_tofrom_post

    if [ $DEBUG -eq 1 ] ; then
        echo "After:"
        display_activation_constellation ac_tofrom_post
    fi

    check_activation_combo ac_tofrom_post
    if [ $? -ne $success ] ; then
        exitc 1 "ERROR: invalid combo after switching"
    fi
}

check_get_put() {
    # 1: direction specifier
    enforce_number_of_arguments 1 $# $FUNCNAME
    enforce_inert_host_is_specified
    local direction=$1

    local -A ac_getput
    fill_activation_constellation_structure ac_getput
    local -r ac_getput
    display_activation_constellation ac_getput

    check_activation_combo ac_getput

    case "${direction}" in

        frominerthost)
            if [[ ${ac_getput[inertstate]} != $act_state_on ]] ; then
                exitc 1 "Check failed: inert host $inert_host is not active"
            fi
            ;;

        toinerthost)
            if [[ ${ac_getput[operstate]} != $act_state_on ]] ; then
                exitc 1 "Check failed: operative host $operative_host"\
                    "is not active"
            fi
            ;;

        *)
            exitc 1 "Error: unrecognized direction specifier '$direction'"
            ;;
    esac
}

check_combo(){
    enforce_number_of_arguments 0 $# $FUNCNAME
    enforce_inert_host_is_specified

    local -A ac_checkcombo
    fill_activation_constellation_structure ac_checkcombo
    local -r ac_checkcombo
    display_activation_constellation ac_checkcombo

    check_activation_combo ac_checkcombo
    exit $?
}

check_event_tokens() {
    # 1: nonempty-yes whether a resolution should be tried
    enforce_number_of_arguments 1 $# $FUNCNAME
    local resolve_q="$1"

    local -A et_check
    read_event_tokens et_check
    local -r et_check
    display_event_tokens et_check

    check_event_tokens_and_resolve_cond et_check "$resolve_q"
}

show_status() {
    enforce_number_of_arguments 0 $# $FUNCNAME

    local -A et_status
    read_event_tokens et_status
    local -r et_status

    local no_resolution=""
    check_event_tokens_and_resolve_cond et_status "$no_resolution"

    local status_s="${AS_display_name[$act_state_on]}"
    local check_v=""
    if [ "${et_status[active]}" != "`hostname`" ] ; then
        check_v=" (not verified)"
    fi

    echo "${et_status[active]} is ${status_s}${check_v}"
}

force_active_localhost() {
    # Put localhost in forced state. Useful when detached.
    local -A et
    read_event_tokens et
    local -r et

    # Well, ...
    local msg_on=${AS_display_name[$act_state_on]}
    local msg_off=${AS_display_name[$act_state_off]}

    enforce_clean_state et

    get_activation_state $thishost
    act_state_before=$?

    if [ $act_state_before -eq $act_state_on ] ; then
        if [ "${et[active]}" == "$thishost" ] ; then
            notify "Host $thishost is already ${msg_on}."
            exit 2
        else
            exitc 1 "Invalid state: Host $thishost is already"\
                "$msg_on but not recorded as such."
        fi
    elif [ "${et[active]}" == "$thishost" ] ; then
        exitc 1 "Invalid state: $thishost recorded as $msg_on but is $msg_off"
    fi

    switch_device_actstate $thishost $act_state_on
    write_event_tokens ${et[active]} $thishost

    notify "Forced on localhost $thishost"
    notify "Consider to turn on NAS to get up to date"
}

brute_active() {
    # Records messed up. Brutalize localhost to be active and then monitor other
    # devices for errors and turn themselves off. Errors instead of resolution.
    # But it's just a precaution anyway -- and yet very long.
    local answer
    cecho white red "This will wipe existing records. Are you sure? (yes|*)"
    read answer
    if [[ $answer != yes ]] ; then
        echo "Aborting. Not exactly 'yes'."
        exit $failure
    fi

    set -e
    mkdir -p "$AS_folder" "$ET_folder"
    rm -f "$AS_full_path_on" "$AS_full_path_off"
    ln -s "$AS_link_target" "$AS_full_path_on"
    write_event_tokens $thishost ""
    set +e
    notify "$thishost is a brute. See if another active device fixes itself."
}

init_device() {
    if [ -e "$AS_folder" ] ; then
        echo "This device seems to be already initialized. Use brute-active"\
            "if you want to wipe and restore broken records."
        exit $failure
    fi

    set -e
    if [[ ! -e "$ET_full_path" ]] ; then
        echo "Creating state and token records where $thishost is active..."
        mkdir -p "$AS_folder" "$ET_folder"
        ln -s "$AS_link_target" "$AS_full_path_on"
        write_event_tokens $thishost ""
    else
        echo "Shared token found. Initializing $thishost as inactive..."
        mkdir -p "$AS_folder"
        ln -s "$AS_link_target" "$AS_full_path_off"
    fi
    set +e
}

#---
#--- Combo Policy
#================
#---

assign_interlocutors() {
    # Assign operative and inert host from the configuration
    enforce_number_of_arguments 0 $# $FUNCNAME

    if [ -n "$operative_host_cfg" ] ; then
        operative_host="$operative_host_cfg"
        if [[ $operative_host != `hostname` ]] ; then
            echo "Operative host is '$operative_host' and not"\
                "localhost '`hostname`'"
        fi
        debug_out "operative_host is '$operative_host' as per configuration"
    else
        operative_host=`hostname`
        debug_out "operative_host is localhost '$operative_host'"
    fi

    [[ -n "${operative_host}" ]] || exitc 1 "Can't determine operative_host"

    if [[ $inert_host_cfg == $operative_host ]] ; then
        # leave inert_host unspecified if it would be equal to the
        # operative_host so that the inert host can be operative for operations
        # that don't require an interlocutor.
        inert_host=""
        debug_out "inert host left empty. Equal to operative_host"\
            "'$operative_host'"
    else
        inert_host="$inert_host_cfg"
        debug_out "inert host is '$inert_host' from config"
    fi

    if [[ $operative_host == $inert_host ]] ; then
        exitc 1 "The inert host cannot be equal to the operative host"\
            "'$operative_host'"
    fi
}

#-------------- sequence ---------------
#=======================================

[ -n "$1" ] || usage 10
subcommand="$1"
shift

declare inert_host=""
declare operative_host=""
assign_interlocutors

#--- Sub-commands

case "$subcommand" in

    -h|--help|help) # this help message
        help_output
        exit 0
        ;;

#
# ======= Commands =========
#
# = Pair Transition (requires interlocutor):
#

    get) # get the activation state from the inert host
        to_from_inerthost frominerthost
        ;;

    put) # put the activation state to the inert host
        to_from_inerthost toinerthost
        ;;

    check-get) # preliminary test to match expectations for get
        # Use this together with get/put to bracket your additional leave/come
        # actions
        check_get_put frominerthost
        ;;

    check-put) # preliminary test to match expectations for put
        # Use this together with get/put to bracket your additional leave/come
        # actions
        check_get_put toinerthost
        ;;

    combo) # display and check the activation state of interlocutors
        check_combo
        ;;

#
# = Event Tokens:
#

    status|s) # Show who's on and check if localhost's state matches
        show_status
        ;;

    token) # Display tokens and check whether localhost complies
        check_event_tokens ""
        ;;

    resolve) # Resolve a forced state. Used in the systemd handler
        check_event_tokens do-resolve
        ;;

#
# = Record mangling:
#

    force-active) # Force an active state of localhost
        force_active_localhost
        ;;

    brute-active-beware) # Fix broken records. Resolve with errors basically
        # Use this only when really necessary. Restores the active link and
        # writes a token record for localhost. Other devices turn themselves off
        # but issue an error message.
        brute_active
        ;;

    init-device) # Start a device fleet or join inactive if the token is present
        # Write the active state and token records for this device or start
        # inactive if the shared token is found.
        init_device
        ;;

#
# = Utility:
#

    journal) # check the output of the handler in journalctl
        journalctl --no-tail -f -b | grep "$programname"
        ;;

#
# - (Hint: Set DEBUG=1 to show more detailed output)

#--- testing

    test-gui-msg)
        exec 0</dev/zero
        notify "test: and some more news"
        exitc 1 "test: and some more news"
        ;;

    *)
        exitc 1 "No valid argument/sub-command given"
        ;;

esac

exit $success
