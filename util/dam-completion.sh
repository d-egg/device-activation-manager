#!/bin/bash

__dam_location_for_completion="`which device-activation.sh`"

__dam_subcmd_shell_completion() {
    local dist_cmds prefix
    read -r -d '' dist_cmds <<-EOF
        `grep -e ')\ # ' "$__dam_location_for_completion" \
            | sed 's/^\s*\([a-zA-Z|_-]\+\).*/\1/g;s/|/\n/g' \
        `
EOF
    prefix=${COMP_WORDS[COMP_CWORD]}
    COMPREPLY=( `echo "${dist_cmds}" | grep -o "^\s*${prefix}\S*"` )
}

if [ -n "$debug_dam_shell_completion" ] ; then
    COMP_WORDS[COMP_CWORD]=$1
    __dam_subcmd_shell_completion
    echo
    echo "--- compreply:"
    echo ${COMPREPLY[@]}
fi
