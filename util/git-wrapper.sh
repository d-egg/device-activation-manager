#!/bin/bash
if [ -e ~/active-device/syncer.off ] ; then
    echo "$0: git is turned off by the device activation" >&2
    exit 128
else
    exec /usr/bin/git "$@"
fi
