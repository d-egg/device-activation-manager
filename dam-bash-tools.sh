EXIT_SUCCESS=0
EXIT_FAILURE=1
EXIT_ERROR=2
EXIT_BUG=10

cecho() {
    # USAGE: cecho <fg-color> <bg-color> "<echo-string>"
    [[ ${#} < 3 ]] && (echo "cecho : not enough args" >&2 ; exit 1)

    if [[ $TERM =~ unknown|dumb || $TERM == "" ]] ; then
        shift 2
        echo "$@"
        return
    fi

    local lgrey=0
    local black=0

    local lred=1
    local red=1

    local lgreen=2
    local green=2

    local yellow=3
    local orange=3

    local lblue=4
    local blue=4

    local pink=5
    local magenta=5

    local lcyan=6
    local cyan=6

    local white=7
    local grey=7

    # Setting f-oreground and b-ackground
    tput setaf ${!1}
    tput setab ${!2}
    case $1 in
        lgrey|lred|lgreen|yellow|lblue|pink|lcyan|white)
                tput bold
        ;;
    esac
    shift 2
    # Texting
    echo -n "$@"
    tput op
    tput sgr0
    echo
}

# wrapper around cecho to write to stderr (test)
cerror() {
    exec 9<&1
    exec 1>&2
    cecho "$@"
    exec 1<&9
}

cexit() {
    # eexit <errval> [exit-text]
    # Gibt exit-text aus und beendet mit <errval>
    ERRVAL=$1
    shift 1
    cerror white red "$@"
    exit $ERRVAL
}

# debug_out "text"
debug_out() {
    [[ $DEBUG -ne 1 ]] && return
    cecho magenta white "$@" >&2
}
